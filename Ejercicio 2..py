# Autora Cinthia Valdez
# Elaboracion del ejercicio dos del capitulo cinco
# Ejercicio 2: Escribe otro programa que pida una
# lista de números como la anterior y al ﬁnal muestre
# por pantalla el máximo y mínimo de los números, en vez de la media.
acu = 0
contador = 0
lista = []
while True:
    valor = input("Ingrese un valor")
    lista.append(valor)
    if valor == "fin":
        break
    try:
        acu = acu + int(valor)
        contador = contador + 1
        maximo = max(lista)
        minimo = min(lista)
    except:
        print("invalido")
print("la suma de los números es:", acu)
print("la cantidad de número es:", contador)
print("El número minimo es :", float(minimo))
print("El número maximo es: ", float(maximo))