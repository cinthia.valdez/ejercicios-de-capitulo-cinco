# Autora Cinthia Valdez
# elaboracion de ejercicio uno del capitulo cinco
# Ejercicio 1: Escribe un programa que lea repetidamente números hasta que el usuario introduzca “ﬁn”.
# Una vez se haya introducido “ﬁn”, muestra por pantalla el total, la cantidad de números y la media de
# esos números. Si el usuario introduce cualquier otra cosa que no sea un número, detecta su fallo
# usando try y except, muestra un mensaje de error y pasa al número siguiente.
acu = 0
contador = 0
while True:
    try :
        valor = input("Ingrese un valor")
        acu = acu + int(valor)
        contador = contador + 1
    except:
        if valor == "fin":
            break
        print ("invalido")
print("la suma de los números es:", acu)
print("la cantidad de número es:", contador)
print("la media de los números es:", acu/contador)